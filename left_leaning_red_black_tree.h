#ifndef __LEFT_LEANING_RED_BLACK_TREE_H__
#define __LEFT_LEANING_RED_BLACK_TREE_H__

#include <string.h> // memcpy
#include <stdio.h>
#include <stdlib.h> // atoi
#include <assert.h>
#include <sys/time.h> // gettimeofday
#include "length_array.h"

// #define PRINT_MESSAGE
// #define GENERATE_INTERNAL_DOT_FILE


// *************************************
typedef enum LinkColor
{
  PLACEHOLDER = 0,
  LINK_COLOR_BLACK = 1,
  LINK_COLOR_RED = 2,
  LINK_COLOR_NONE = 3
} LinkColor;
static int rotation_count = 0;


// G.M. Adelson-Velsky
// E.M. Landis
typedef struct node_t
{
  struct node_t *parent;
  struct node_t *left;
  struct node_t *right;
  LinkColor left_color;
  LinkColor right_color;
  int data;
} node_t;


// when last==0, return the pointer to the node
// when last==1, return the pointer to the parent of the node
node_t *new_node (void);
void insert (node_t *root, node_t *leaf);
node_t *delete_node (node_t *root, node_t *leaf);
node_t *tree_search_data (node_t *root, int data, int generation);
node_t *tree_search (node_t *root, node_t *leaf, int generation);
int get_depth_of_node (node_t *leaf);
node_t *rotate (node_t *root, int direction);
node_t *delete_node_and_sub_nodes (node_t *root, node_t *leaf);
node_t *find_node_and_rotate (node_t *root, node_t *leaf, int direction);
void generate_dot_file (node_t *root, char *filename);
void generate_dot_file_with_current_time (node_t *root, char *filename);
void flip_node_color (node_t *root, node_t *parent);
int is_tree_valid (node_t *root);
int is_tree_valid_value_order_right (node_t *root);
int is_tree_valid_double_side_link_identical (node_t *root);
int is_tree_valid_two_red_children_check (node_t *root);
int is_tree_valid_leaning_left_check (node_t *root);
int is_tree_valid_black_branch_balance_check (node_t *root);
void print_tree_linearly (const node_t *tree, LengthArray *a);
LinkColor flip_link_color (LinkColor color);
int get_depth_of_tree_black_link (node_t *root, node_t *leaf);
node_t *normalize_tree (node_t *root, node_t *leaf);
void connect_nodes (node_t *parent, node_t *child);

#endif /* __LEFT_LEANING_RED_BLACK_TREE_H__ */
