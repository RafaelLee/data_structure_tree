# Left Leaning Red Black Tree with tree visualization
## This version is not tidy and robust.
### If you need better and full functional code, email me. I'll rewrite the code.


These material are very helpful:
https://www.cs.princeton.edu/~rs/talks/LLRB/RedBlack.pdf
https://www.cs.swarthmore.edu/~adanner/cs35/s10/LLRB.pdf

make  


./main  


dot_2_png.py  


make coverage  


The coverage report coverage/index.html will generate  


![](doc/rb_tree.png)
