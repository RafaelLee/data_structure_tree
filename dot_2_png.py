#!/usr/bin/python3
import os
import sys


files=os.listdir()

files_new = []
for i in files:
    if (i.lower().endswith(".dot")):
        files_new.append(i)

# print(files_new)

files_new.sort()

for i in files_new:
    new_file_name = os.path.splitext(i)[0]+'.png'
    cmd = "dot -T png -o %s %s"%(new_file_name, i)
    os.system(cmd)
