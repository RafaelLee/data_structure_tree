#include <assert.h>
#include <stdio.h>
#include "left_leaning_red_black_tree.h"

int main()
{
  // int a = stdout;
  // int a[] = {6, 4, 2, 8, 1, 4, 9, 0, 3, 2};
  int a[] = {39, 95, 237, 183, 43, 816, 894, 529, 308, 212, 557, 749, 286, 328, 4, 491, 582, 839, 68, 842, 91, 397, 60, 899, 66, 138, 400, 693, 29, 658, 400, 969, 269, 140, 563, 384, 21, 135, 454, 560, 995, 968, 987, 592, 438, 16, 100, 591, 736, 252, 12, 143, 670, 418, 731, 350, 74, 964, 276, 516, 999, 812, 230, 189, 843, 230, 985, 980, 808, 581, 852, 889, 676, 985, 174, 614, 757, 291, 493, 346, 407, 577, 39, 143, 639, 910, 683, 356, 462, 111, 281, 487, 805, 493, 266, 552, 687, 562, 792, 305};
  // int a[] = {6, 4, 2, 8, 1, 4, 9, 0, 3, 2};
  // int a[] = {5, 3, 3, 0, 8, 6, 3, 9, 2, 8, 2, 7, 8};
  // int a[] = {2, 2, 3, 6, 0, 7, 4, 9, 3, 0, 1, 1, 6, 9, 8, 1, 4, 3, 8, 5};
  // int a[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63};
  // int a[] = {3, 2, 1};

  node_t *root;
  node_t *leaf;

  int len = sizeof (a) / sizeof (int);
  root = new_node();
  // memcpy (&(root->data), &(a[0]), sizeof (int));
  root->data = a[0];
  for (int i = 1; i < len; i++)
  {
    // remove duplicated
    if (!tree_search_data (root, a[i], 1))
    {
      leaf = new_node();
      leaf->data = a[i];
      insert (root, leaf);

      // rotate print
      generate_dot_file_with_current_time (root, (char *)".1.after_insert");
      // rotation_count++;

      // root = find_node_and_rotate (root, child_leaf, 0); // rotate left
#ifdef PRINT_MESSAGE
      if (0 != root)
      {
        if (0 != is_tree_valid (root))
        {printf ("before normalize vvvvvvvvvv\n");}
      }
#endif

      // is_tree_valid (root);
      root = normalize_tree (root, leaf);

      generate_dot_file_with_current_time (root, (char *)".8.after_normalize");

      if (0 != root)
      {assert (0 == is_tree_valid (root) && "LLRB Tree is invalid");}

      rotation_count++;
    }
    else
    {printf ("%d, duplicated data detected, not insert to tree\n", i);}
  }
  printf ("\n");
  printf ("tree depth is %d\n", get_depth_of_node (root));
  printf ("0 has depth of %d\n", get_depth_of_node (0));
  generate_dot_file_with_current_time (root, (char *)".9._after_insert");

  // check if the tree is a valid BST(binary search tree)
  LengthArray *a0 = length_array_create();
  print_tree_linearly (root, a0);
  for (size_t i = 0; i < a0->tail; i++)
  {
    printf ("%d, ", tree_search_data (root, a0->array[i], 0)->data);
  }
  printf ("\n");
  length_array_delete (a0);

  LengthArray *a1 = length_array_create();
  print_tree_linearly (root, a1);
  for (size_t i = 0; i < a1->tail; i++)
  {printf ("%d, ", a1->array[i]);}
  printf ("\n");
  length_array_delete (a1);

  generate_dot_file (root, (char *)"00.0.dot");

  // exist
  assert (0 == is_tree_valid (root) && "tree is invalid before delete_node");
  root = delete_node (root, tree_search_data (root, 639, 1));
  assert (0 == is_tree_valid (root) && "tree is invalid after delete_node");
  generate_dot_file (root, (char *)"00.1.delete.639.dot");

  root = delete_node (root, tree_search_data (root, 805, 1));
  assert (0 == is_tree_valid (root) && "tree is invalid after delete_node");
  generate_dot_file (root, (char *)"00.1.delete.805.dot");

  // not exist
  root = delete_node (root, tree_search_data (root, 640, 1));
  assert (0 == is_tree_valid (root) && "tree is invalid after delete non-existing node");
  generate_dot_file (root, (char *)"00.2.delete.non-exist.640.dot");

  root = delete_node (root, tree_search_data (root, 658, 1));
  assert (0 == is_tree_valid (root) && "tree is invalid after delete non-existing node");
  generate_dot_file (root, (char *)"00.3.delete.658.dot");

  root = delete_node (root, tree_search_data (root, 670, 1));
  assert (0 == is_tree_valid (root) && "tree is invalid after delete non-existing node");
  generate_dot_file (root, (char *)"00.4.delete.670.dot");

  root = delete_node (root, root);
  assert (0 == is_tree_valid (root) && "tree is invalid after delete non-existing node");
  generate_dot_file (root, (char *)"00.5.delete.root.dot");

  // delete all nodes under, after delete, the LLRB maybe invalid
  root = delete_node_and_sub_nodes (root, tree_search_data (root, 183, 1));
  // fill all
  generate_dot_file (root, (char *)"09.0.delete_183.dot");
  assert (0 == is_tree_valid (root) && "after delete_node_and_sub_nodes, tree invalid");

  LengthArray *a2 = length_array_create();
  print_tree_linearly (root, a2);
  for (size_t i = 0; i < a2->tail; i++)
  {
#ifdef PRINT_MESSAGE
    printf ("delete node %d, ", a2->array[i]);
#endif
    root = delete_node (root, tree_search_data (root, a2->array[i], 0));
    char filename[81] = "";
    memset (filename, 0, 81);
    sprintf (filename, "01.%02ld.delete.%d.dot", i, a2->array[i]);
    if (0 != root)
    {
#ifdef PRINT_MESSAGE
      printf ("root=%p, ", root);
      printf ("root->data=%d\n", root->data);
#endif
      generate_dot_file (root, filename);
    }
    else
    {
      printf ("tree is empty cannot print\n");
    }
  }
  length_array_delete (a2);

  // cannot assign root equals to the return of this function, since the input is 0
  printf ("root=0, root = %p\n", root);
  delete_node_and_sub_nodes (0, root);

  printf ("leaf=0, root = %p\n", root);
  root = delete_node_and_sub_nodes (root, 0);

  // explicitly delete root is required, frees memory by hand

  return 0;
}
