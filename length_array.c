#include "length_array.h"

void length_array_realloc (LengthArray *a, size_t size)
{
  a->array = (int *)realloc (a->array, sizeof (int) * size);
  if (0 == a->array)
  {exit (-1);}
  a->len = size;
}


LengthArray *length_array_create (void)
{
  LengthArray *a = (LengthArray *)malloc (sizeof (LengthArray));
  a->len = 0;
  a->tail = 0;
  a->array = 0;
  return a;
}


void length_array_delete (LengthArray *a)
{
  free (a->array);
  free (a);
}


void length_array_push_back (LengthArray *a, int v)
{
  a->tail++;
  if (a->tail >= a->len)
  {
    int delta = (0 == a->len) ? 1 : (a->len);
    length_array_realloc (a, delta + a->len);
  }
  a->array[a->tail - 1] = v;
}


int length_array_pop_back (LengthArray *a)
{
  if (0 == a->tail)
  {
    fprintf (stderr, "tail less than 0, cannot pop\n");
    exit (-1);
  }
  int value = (a->array)[a->tail - 1];
  if (a->tail < (a->len / 2))
  {
    length_array_realloc (a, (a->len) / 2);
  }
  a->tail--;
  a->len--;
  return value;
}


int length_array_max_element (LengthArray *a)
{
  int max = a->array[0];
  for (size_t i = 1; i < a->tail; i++)
  {
    if (max < a->array[i])
    {
      max = a->array[i];
    }
  }
  return max;
}


int length_array_min_element (LengthArray *a)
{
  int min = a->array[0];
  for (size_t i = 1; i < a->tail; i++)
  {
    if (min > a->array[i])
    {
      min = a->array[i];
    }
  }
  return min;
}
