#include "left_leaning_red_black_tree.h"



int is_tree_valid (node_t *root)
{
  if (0 == root)
  {return 0;}

  int error = 0;
  if (0 != is_tree_valid_value_order_right (root))
  {
    printf ("Tree is not ascending \n");
    error |= 1;
  }
  if (0 != is_tree_valid_double_side_link_identical (root))
  {
    printf ("Tree invalid, double sided linked list error\n");
    error |= 2;
  }
  if (0 != is_tree_valid_two_red_children_check (root))
  {
    printf ("Tree invalid, node have 2 red children\n");
    error |= 4;
  }
  if (0 != is_tree_valid_leaning_left_check (root))
  {
    printf ("Tree invalid, not leaning left\n");
    error |= 8;
  }
  if (0 != is_tree_valid_black_branch_balance_check (root))
  {
    printf ("Tree invalid, not leaning left\n");
    error |= 16;
  }
  error = -1 * error;
  if (error != 0)
  {
    printf ("error = %d\n", error);
  }
  return error;
}


int is_tree_valid_value_order_right (node_t *root)
{
  LengthArray *v = length_array_create();
  print_tree_linearly (root, v);
  int last = v->array[0];
  for (size_t i = 1; i < v->tail; i++)
  {
    if (last >= v->array[i])
    {
      return -1;
    }
  }
  length_array_delete (v);
  return 0;
}


int is_tree_valid_double_side_link_identical (node_t *root)
{
  node_t *child = root;
  if (0 != child->left)
  {
    if (child->left->parent != child)
    {
      printf ("double sided link isn't identical, node is %p, left is %p, left->parent is %p\n", child, child->left, child->left->parent);
      return -1;
    }
    int result = is_tree_valid_double_side_link_identical (child->left);
    if (0 != result)
    {
      printf ("double sided link isn't identical, node is %p, left is %p, left->parent is %p\n", child, child->left, child->left->parent);
      return result;
    }
  }
  if (0 != child->right)
  {
    if (child->right->parent != child)
    {
      printf ("double sided link isn't identical, node is %p, right is %p, right->parent is %p\n", child, child->right, child->right->parent);
      return -1;
    }
    int result = is_tree_valid_double_side_link_identical (child->right);
    if (0 != result)
    {
      printf ("double sided link isn't identical, node is %p, right is %p, right->parent is %p\n", child, child->right, child->right->parent);
      return result;
    }
  }
  return 0;
}


int is_tree_valid_two_red_children_check (node_t *root)
{
  node_t *child = root;
  if (0 == root)
  {return 0;}

  if (0 != child->left)
  {
    int result = is_tree_valid_two_red_children_check (child->left);
    if (0 != result)
    {
      printf ("node has 2 red children, node is %p, left is %p, right is %p\n", child, child->left, child->right);
      return result;
    }
  }
  if (0 != child->right)
  {
    int result = is_tree_valid_two_red_children_check (child->left);
    if (0 != result)
    {
      printf ("node has 2 red children, node is %p, left is %p, right is %p\n", child, child->left, child->right);
      return result;
    }
  }
  if ((0 != child->right) && (0 != child->left))
  {
    if (child->left_color == LINK_COLOR_RED && child->right_color == LINK_COLOR_RED)
    {
      return -1;
    }
  }

  return 0;
}


int is_tree_valid_leaning_left_check (node_t *root)
{
  node_t *child = root;
  if (0 != child->right)
  {
    if (LINK_COLOR_RED == child->right_color)
    {
      return -1;
    }
  }

  if (0 != child->left)
  {
    int result = is_tree_valid_leaning_left_check (child->left);
    if (0 != result)
    {
      return result;
    }
  }

  if (0 != child->right)
  {
    int result = is_tree_valid_leaning_left_check (child->right);
    if (0 != result)
    {
      return result;
    }
  }

  return 0;
}


void is_tree_valid_black_branch_balance_check_loop (node_t *root, node_t *child, LengthArray *v)
{
  if (0 != child->left)
  {
    is_tree_valid_black_branch_balance_check_loop (root, child->left, v);
  }

  if (0 == child->left && 0 == child->right)
  {
    int black_link_count = get_depth_of_tree_black_link (root, child);
    length_array_push_back (v, black_link_count);
  }

  if (0 != child->right)
  {
    is_tree_valid_black_branch_balance_check_loop (root, child->right, v);
  }
}


int is_tree_valid_black_branch_balance_check (node_t *root)
{
  // v leaf_node_black_link_count;
  LengthArray *v = length_array_create();
  node_t *child = root;
  is_tree_valid_black_branch_balance_check_loop (root, child, v);
  int max = length_array_max_element (v);
  int min = length_array_min_element (v);

#ifdef PRINT_MESSAGE
  printf ("************* ");
  for (size_t i = 0; i < v->tail; i++)
  {printf ("%d, ", v->array[i]);}
  printf (" ************* black link count\n");
#endif
  length_array_delete (v);

  if (max - min <= 2)
  {return 0;}
  else
  {return -1;}
}


void connect_nodes (node_t *parent, node_t *child)
{
  child->parent = parent;
  if (child->data > parent->data)
  {
    parent->right = child;
  }
  else
  {
    parent->left = child;
  }
}


node_t *delete_node (node_t *root, node_t *leaf)
{
  if (0 == root || 0 == leaf)
  {return root;}
#ifdef PRINT_MESSAGE
  printf ("delete leaf->data==%d\n", leaf->data);
#endif

  // node has no child, delete directly
  if (0 == leaf->left && 0 == leaf->right)
  {
    root = delete_node_and_sub_nodes (root, leaf);
    return root;
  }

  // node has only one child, connect the child to parent
  // then delete the node
  // after new node inserted this situation always occurs at the bottom
  // but it will occur in non-leaf nodes when delete node from tree
  else if (0 == leaf->left)
  {
    if (root == leaf)
    {
      root->right->parent = 0;
      root = root->right;
    }
    else
    {
      node_t *parent = tree_search (root, leaf, 1);
      connect_nodes (parent, leaf->right);
    }

    free (leaf);

    return root;
  }
  else if (0 == leaf->right)
  {
    if (root == leaf)
    {
      root->left->parent = 0;
      root = root->left;
    }
    else
    {
      node_t *parent = tree_search (root, leaf, 1);
      connect_nodes (parent, leaf->left);
    }

    // address of node
    free (leaf);

    return root;
  }

  // node has 2 children
  // since the tree is left leaning, so delete the biggest in the left child tree is better
  node_t *child = leaf->left;
  while (0 != child->right)
  {
    child = child->right;
  }
  int temp = child->data;

  node_t *parent = tree_search (root, child, 1);
  // the tree cannot store duplicated value, so the data must assign after delete
  // tree_search is invoked in delete_node_and_sub_nodes
  // and tree_search doesn't support duplicated value in tree
  root = delete_node_and_sub_nodes (root, child);
  leaf->data = temp;

  if (0 != parent->right)
  {root = normalize_tree (root, parent->right);}
  else if (0 != parent->left)
  {root = normalize_tree (root, parent->left);}
  else
  {root = normalize_tree (root, parent);}

  return root;
}


node_t *delete_node_and_sub_nodes (node_t *root, node_t *leaf)
{
  if (0 == root)
  {return root;}
  node_t *new_root = root;
  if (leaf == 0)
  {return root;}
  node_t *parent_node;
  if (leaf != root)
  {parent_node = tree_search_data (root, leaf->data, 1);}
  else // leaf == root
  {
    new_root = 0;
    parent_node = 0;
  }

  node_t *child_leaf = leaf;
  // delete left child
  if (child_leaf->left != 0)
  {root = delete_node_and_sub_nodes (root, child_leaf->left);}

  // delete right child
  if (child_leaf->right != 0)
  {root = delete_node_and_sub_nodes (root, child_leaf->right);}

  // delete node from parent
  if (0 != parent_node)
  {
    if (parent_node->left == leaf)
    {parent_node->left = 0;}
    else if (parent_node->right == leaf)
    {parent_node->right = 0;}
    else
    {assert (0 && "this cannot happen");}
  }

  // address of node
  free (leaf);
  if (0 == new_root)
  {return new_root;}
  return root;
}


node_t *tree_search (node_t *root, node_t *leaf, int generation)
{
  node_t *p = leaf;
  for (int i = generation; i > 0; i --)
  {
    p = p->parent;
  }
  return p;
}


// this tree search cannot cover tree with the same data
// but the insert function supports node with the same data
// return 0 generation==1 and data==root->data
node_t *tree_search_data (node_t *root, int data, int generation)
{
  node_t *node_queue[128];
  node_t *child = root;
  node_t *result = 0;

  int layer = 0;
  while (1)
  {
    if (data < child->data)
    {
      if (child->left != 0)
      {
        node_queue[layer] = child;
        layer++;
        child = child->left;
      }
      else
      {return 0;}
    }
    else if (data > child->data)
    {
      if (child->right != 0)
      {
        node_queue[layer] = child;
        layer++;
        child = child->right;
      }
      else
      {return 0;}
    }
    else // equals
    {
      node_queue[layer] = child;
      if (layer - generation < 0)
      { return 0; }
      result = node_queue[layer - generation - 0];
      return result;
    }
  }
}

// how can I return 0 here;
node_t *new_node (void)
{
  node_t *new_node = (node_t *) calloc (1, sizeof (node_t));
  if (0 == new_node)
  {return 0;}
  new_node->parent = 0;
  new_node->left = 0;
  new_node->right = 0;
  new_node->right_color = LINK_COLOR_BLACK;
  new_node->left_color = LINK_COLOR_BLACK;
  new_node->data = -1;
  return new_node;
}


void print_tree_linearly_loop (const node_t *tree, LengthArray *vi)
{
  if (tree->left != 0)
  {print_tree_linearly_loop (tree->left, vi);}

  length_array_push_back (vi, tree->data);

  if (tree->right != 0)
  {print_tree_linearly_loop (tree->right, vi);}
}


void print_tree_linearly (const node_t *tree, LengthArray *a)
{
  print_tree_linearly_loop (tree, a);
}


// supports node with the same data
// but tree_search doesn't
// new node is always under red link
// if there are 2 red occurs continuously, rotate links or flip color
void insert (node_t *root, node_t *leaf)
{
  node_t *child_leaf = root;

  while (1)
  {
    int greater = (leaf->data >= child_leaf->data) ? 1 : 0;
    if (greater)
    {
      if (child_leaf->right == 0)
      {
        child_leaf->right = leaf;
        child_leaf->right_color = LINK_COLOR_RED;
        leaf->parent = child_leaf;

        // //      a          node
        // //   any  red      link
        // // node      new   node

        // //     new         node
        // //   red  any      link
        // //  a        any   node

        break;
      }
      else // empty
      {
        child_leaf = child_leaf->right;
        continue;
      }
    }
    else // lesser
    {
      if (child_leaf->left == 0)
      {
        child_leaf->left = leaf;
        child_leaf->left_color = LINK_COLOR_RED;
        leaf->parent = child_leaf;
        break;
      }
      else // empty
      {
        child_leaf = child_leaf->left;
        continue;
      }
    }
  }
}


int get_depth_of_tree_black_link (node_t *root, node_t *leaf)
{
  node_t *child = leaf;
  node_t *last = leaf;
  int cnt = 0;
  while (child != root)
  {
    // left
    if (last->data < child->data)
    {
      if (child->left_color == LINK_COLOR_BLACK)
      {cnt++;}
    }
    // right
    else
    {
      if (child->right_color == LINK_COLOR_BLACK)
      {cnt++;}
    }
    child = tree_search (root, child, 1);
    // child = child->parent;
  }
  return cnt;
}


int get_depth_of_node (node_t *leaf)
{
  if (0 == leaf)
  {return 0;}
  int depth_left;
  int depth_right;
  if (0 == leaf->left)
  {depth_left = 0;}
  else
  {depth_left = get_depth_of_node (leaf->left);}

  if (0 == leaf->right)
  {depth_right = 0;}
  else
  {depth_right = get_depth_of_node (leaf->right);}

  if (depth_left > depth_right)
  {return depth_left + 1;}
  else
  {return depth_right + 1;}
}


node_t *rotate (node_t *leaf, int direction)
{
  // 0 for rotate left, 1 for rotate right
  if (1 == direction)
  {
    if (leaf->left != 0)  // && leaf->left->right == 0)
    {
      //   a               b
      //  b       ->     c   a
      // c d                d
      node_t *a, *b, *c, *d;
      a = leaf;
      b = leaf->left;
      c = leaf->left->left;
      d = leaf->left->right;
      LinkColor a_left_color_previous = a->left_color;

      node_t *temp_parent = leaf->parent;
      a->left = d;
      a->parent = b;
      if (0 != d)
      {d->parent = a;}
      b->right = a;
      if (0 != c)
      { c->parent = b; }
      a->left_color = b->right_color;
      b->right_color = a_left_color_previous;
      b->parent = temp_parent;

      return b;
    }
    else
    {return leaf;}
  }
  else // rotate left
  {
    if (leaf->right != 0)
    {
      //  a                b
      //    b     ->     a   c
      //   d c            d

      node_t *a, *b, *c, *d;
      a = leaf;
      b = leaf->right;
      c = leaf->right->right;
      d = leaf->right->left;
      LinkColor a_right_color_previous = a->right_color;

      node_t *temp_parent = leaf->parent;
      a->right = d;
      a->parent = b;
      b->left = a;
      a->right_color = b->left_color;
      b->left_color = a_right_color_previous;
      if (0 != d)
      {d->parent = a;}
      if (0 != c)
      { c->parent = b; }
      b->parent = temp_parent;

      return b;
    }
    else
    {return leaf;}
  }
}


void generate_dot_file (node_t *root, char *filename)
{
  FILE *fd;
  fd = fopen (filename, "w");
  if (!fd)
  {exit (-1);}

  LengthArray *a = length_array_create();
  print_tree_linearly (root, a);

  char t0[] = "digraph \"LLRB tree\" {"
              "\n    rankdir=UD;"
              "\n    node [shape=oval];\n";
  fwrite (t0, sizeof (char), strlen (t0), fd);
  char format_string_temp[81]  = {'\0'};
  // use logger to avoid duplicated data in array
  for (size_t i = 0; i < a->tail; i++)
  {
    node_t *parent = tree_search_data (root, a->array[i], 1);
    if (0 != parent)
    {
      char color_red[] = "red";
      char color_black[] = "black";
      char *color;
      if (a->array[i] >= parent->data)
      {color = (parent->right_color == LINK_COLOR_RED) ? color_red : color_black;}
      else
      {color = (parent->left_color == LINK_COLOR_RED) ? color_red : color_black;}

      sprintf (format_string_temp, "    \" %d \" -> \" %d \" [color=%s,label=\"%s\"];\n", parent->data, a->array[i], color, a->array[i] > parent->data ? "\\\\" : "/");

      fwrite (format_string_temp, sizeof (char), strlen (format_string_temp), fd);
    }
  }
  length_array_delete (a);
  fwrite ("}\n", sizeof (char), 2, fd);
  fclose (fd);
}


// return the new root of tree
node_t *find_node_and_rotate (node_t *root, node_t *leaf, int direction)
{
  if (leaf == 0 || root == 0)
  {return root;}
  if (root == leaf)
  {return rotate (leaf, direction);}
  else // root != leaf
  {
    node_t *base = tree_search (root, leaf, 1);
    if (base->data < leaf->data)
    {base->right = rotate (leaf, direction);}
    else
    {base->left = rotate (leaf, direction);}
    return root;
  }
}


LinkColor flip_link_color (LinkColor color)
{
  if (color == LINK_COLOR_RED)
  {return LINK_COLOR_BLACK;}
  else
  {return LINK_COLOR_RED;}
}


void flip_node_color (node_t *root, node_t *node)
{
  node_t *parent = tree_search (root, node, 1);
  if (0 != parent)
  {
    LinkColor *parent_link_color;
    {
      if (node->data >= parent->data)
      {parent_link_color = &parent->right_color;}
      else
      {parent_link_color = &parent->left_color;}
    }

    *parent_link_color = flip_link_color (*parent_link_color);
  }
  node->left_color = flip_link_color (node->left_color);
  node->right_color = flip_link_color (node->right_color);
}


// if root is the node, return LinkColor::NONE
LinkColor get_parent_link_color (node_t *root, node_t *node)
{
  if (0 == node)
  {return LINK_COLOR_NONE;}

  node_t *parent = tree_search (root, node, 1);
  LinkColor color = LINK_COLOR_NONE;
  if (0 == parent)
  {return LINK_COLOR_NONE;}
  if (node->data == parent->data)
  {
    assert ("not possible " && __FILE__ && __LINE__ && 0);
    return color;
  }
  // this node is the left child of parent
  else if (node->data < parent->data)
  {return parent->left_color;}
  else if (node->data > parent->data)
  {return parent->right_color;}
  else
  {assert ("get_parent_link_color this is not possible" && __FILE__ && __LINE__ && 0);}
}


long int get_microseconds_since_epoch()
{
  struct timeval time;
  gettimeofday (&time, NULL);
  long micro_seconds = (time.tv_sec * 1000000) + (time.tv_usec);
  return micro_seconds;
}


LinkColor get_the_other_parent_link_color (node_t *root, node_t *node)
{
  node_t *parent = tree_search (root, node, 1);
  LinkColor color = LINK_COLOR_NONE;
  if (0 == parent)
  {return LINK_COLOR_NONE;}
  if (root == node)
  {return LINK_COLOR_NONE;}

  if (node->data == parent->data)
  {return color;}

  else if (node->data < parent->data)
  {return parent->right_color;}
  else // (node->data > parent->data)
  {return parent->left_color;}
}


node_t *normalize_tree (node_t *root, node_t *leaf)
{
  node_t *parent;
  node_t *grand_parent;
  LinkColor parent_link_color;
  LinkColor other_parent_link_color;
  LinkColor grand_parent_link_color;

  if (root == leaf)
  {return root;}

  parent = tree_search (root, leaf, 1);
  // TODO: Rafael Lee
  // should not return, since if parent is root
  if (0 == parent)
  {return root;}

  // the link color of newly inserted node is red
  parent_link_color = get_parent_link_color (root, leaf);
  other_parent_link_color = get_the_other_parent_link_color (root, leaf);

  // step 2:
  // if parent node has 2 red links, then flip node color
  if (parent_link_color == LINK_COLOR_RED && other_parent_link_color == LINK_COLOR_RED)
  {
    flip_node_color (root, parent);
    root = normalize_tree (root, parent);
  }

  // step 3:
  // rotate left if inserted node to right
  parent = tree_search (root, leaf, 1);
  parent_link_color = get_parent_link_color (root, leaf);
  if (parent_link_color == LINK_COLOR_RED)
  {
    // if it's the right node
    if (leaf->data >= parent->data)
    {
      // rotate left
      root = find_node_and_rotate (root, parent, 0);
      leaf = parent;
    }
  }

  parent = tree_search (root, leaf, 1);
  parent_link_color = get_parent_link_color (root, leaf);
  grand_parent_link_color = get_parent_link_color (root, parent);

  // step 4:
  // check if 2 red link in a row and fix it
  if ((parent_link_color == LINK_COLOR_RED) && (grand_parent_link_color == LINK_COLOR_RED))
  {
    grand_parent = tree_search (root, leaf, 2);
    // rotate right
    root = find_node_and_rotate (root, grand_parent, 1);
  }

  other_parent_link_color = get_the_other_parent_link_color (root, leaf);
  parent_link_color = get_parent_link_color (root, leaf);
  if (other_parent_link_color == LINK_COLOR_RED
      && parent_link_color == LINK_COLOR_RED)
  {
    parent = tree_search (root, leaf, 1);
    flip_node_color (root, parent);

    root = normalize_tree (root, parent);
  }

  if (leaf == root)
  {return root;}
  // else
  // {normalize_tree (root, parent);}
  return root;
}


void generate_dot_file_with_current_time (node_t *root, char *filename)
{
  char t[81] = "";
  memset (t, 0, 81);
  sprintf (t, "%ld.%s.dot", get_microseconds_since_epoch(), filename);
  generate_dot_file (root, t);
}
