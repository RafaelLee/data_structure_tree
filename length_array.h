#ifndef __LENGTH_ARRAY_H__
#define __LENGTH_ARRAY_H__
#include <stdio.h>
#include <stdlib.h>
typedef struct LengthArray
{
  size_t len;
  int *array;
  size_t tail;
} LengthArray;

void length_array_realloc (LengthArray *a, size_t size);
LengthArray *length_array_create (void);
void length_array_delete (LengthArray *a);
void length_array_push_back (LengthArray *a, int v);
int length_array_pop_back (LengthArray *a);
int length_array_max_element (LengthArray *a);
int length_array_min_element (LengthArray *a);

#endif
