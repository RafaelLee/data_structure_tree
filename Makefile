CC=gcc

# https://www.gnu.org/software/make/manual/html_node/Implicit-Variables.html

# C flags
# with all warnings, with extra warnings
CFLAGS := -Wall -Wextra -std=c99 -DDEBUG -g  -fsanitize=address -fprofile-arcs -ftest-coverage
# C preprocess flags
CPPFLAGS :=

# Extra flags to give to the C++ compiler.
CXXFLAGS := -Wall -Wextra -std=c++14 -DDEBUG -g -fsanitize=address -fprofile-arcs -ftest-coverage

# -Wl,(XX) means send xx to linker
# -Wl,--gc-sections means send --gc-sections to linker
LDFLAGS := -Wl,--gc-sections -g -fsanitize=address -fprofile-arcs -ftest-coverage

# the first target in makefile is the default target
# which means
# `make` is the same with `make all`

# target: dependencies
# the next line of target starts with tab


TOP := $(shell pwd)/
SRC := .
OBJ := obj
INC := -I$(TOP)

all_source_files := $(sort $(shell find $(SRC) -name "*.c"))
all_o_files := $(all_source_files:.c=.o)
all_o_files := $(addprefix $(OBJ)/,$(all_o_files))
all_d_files := $(all_o_files:.o=.d)


EXECUTABLE_NAME := main
# LD is not using implicit rule
$(EXECUTABLE_NAME): $(all_o_files) $(OBJ)
	# $(CC) $(LDFLAGS) $^ -o $(EXECUTABLE_NAME)
	$(CC) $(LDFLAGS) $(all_o_files) -o $(EXECUTABLE_NAME)

# $@ target
# $< first prerequisite
# $D the directory part of $@
# $^ The names of all the prerequisites, with spaces between them
# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html

# http://make.mad-scientist.net/papers/advanced-auto-dependency-generation/
# https://www.gnu.org/software/make/manual/html_node/Automatic-Prerequisites.html
-include $(all_d_files)

# where to put OBJ is a question, sometimes compile will fail due to sequence error
.PHONY: $(OBJ)
$(OBJ):
	mkdir -p $(OBJ)


$(OBJ)/%.d: %.c
	mkdir -p $(OBJ)
	set -e; rm -f $@; \
	$(CC) -MM $(CFLAGS) $< > $@.$$$$; \
	sed 's|\($*\)\.o[ :]*|\1.o $@ : |g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

# $(CC) -MM $(CXXFLAGS) $< > $@.$$$$; \


# $(OBJ)/%.o: %.c
#	$(CC) $(CFLAGS) $(INC) -c $^
#	# mkdir -p $(OBJ)
#	mv *.o $(OBJ)/ # FIXME: need to find better solution

$(OBJ)/%.o: %.c # $(OBJ)
	$(CC) $(CFLAGS) $(INC) -c $^ # -o$(OBJ)/$($^:.c=.o)
	# $(CC) $(CXXFLAGS) $(INC) -c $^ # -o$(OBJ)/$($^:.cc=.o)
	# mkdir -p $(OBJ)
	mv *.o $(OBJ)/ # FIXME: need to find better solution

%.o:
	echo "please use target obj/$@ instead"
	exit 1

%.d:
	echo "please use target obj/$@ instead"
	exit 1


# A phony target is one that is not really the name of a file
.PHONY: clean
clean:
	rm -f $(EXECUTABLE_NAME)
	rm -rf $(OBJ)
	rm -f *.o
	rm -f *.d
	rm -rf *.dot *.png
	rm -rf *.gcda  *.gcno *.info
	rm -rf coverage
	rm -f GTAGS GRTAGS GPATH

coverage: $(EXECUTABLE_NAME)
	./main
	gcov main
	lcov -c --directory . --output-file $(EXECUTABLE_NAME).info
	genhtml $(EXECUTABLE_NAME).info --output-directory coverage
